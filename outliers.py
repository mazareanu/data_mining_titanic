# Lab 2. Verificam outlierii.
# Tema 2
# Analiza univariata si multivariata.
# La univariata - medie +- 3 sd, IQR
# La bivariate cel putin 2 din alea.
#   Distanta mahalanovis: distanta care ia in calcul si varianta si covarianta + euclidiana
#   Local outlier factor -> bazat pe densitate
#   ...
#   autoencoders - care nu stiu sa fie clasificate de un autoencoder sunt outliere
#   arbori de decizie, dar nu cu information gain ci construiti aleatori care izoleaza outliere.

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
from sklearn.neighbors import LocalOutlierFactor
import scipy
train = pd.read_csv('train.csv')

# Pastram modificarile facute in analiza precedenta.
train['Embarked'] = train['Embarked'].fillna('S')
train['Embarked'] = train['Embarked'].map({'S' : 0, 'C' : 1, 'Q' : 2}).astype(int)
train['Fare'] = train['Fare'].fillna(train['Fare'].median())
train['Sex'] = train['Sex'].map( {'female': 0, 'male': 1} ).astype(int)
train = train.fillna(0)
#Eliminam unele inutile
drop_elements = ['PassengerId', 'Name', 'Ticket', 'Cabin']
train = train.drop(drop_elements, axis = 1)

thyroid = pd.read_csv('thyroid.csv')

DATASETS =  [
    {"label" : "all_data", "dataset": train}, 
    {"label" : "only_survived", "dataset" : train.loc[train['Survived'] == 1]}, 
    {"label" : "only_deceased", "dataset" : train.loc[train['Survived'] == 0]},
    {"label" : "thyroid", "dataset": thyroid}
]

for _ in DATASETS:
    dataset = _.get("dataset")
    label = _.get("label")
    for key in dataset:
        # 1.5IQR
        freq = {}
        first_q, third_q = dataset[key].quantile([0.25, 0.75])
        IQR = third_q - first_q
        plt.close('all')
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_title('Outliere %s; Set %s'%(key, label), fontsize=20)
        tmp = dataset[key].to_frame()

        for instance in dataset[key]:
            c = 'g'
            if not first_q - 1.5 * IQR <= instance <= third_q + 1.5 * IQR:
                c = 'r'
            ax.scatter(instance, freq.get(instance, 0), c=c, s=50)
            freq[instance] = freq.get(instance, 0) + 0.1
        plt.savefig('./plots/outliers_iqr_%s_%s.jpg'%(key,label))
for _ in DATASETS:
    dataset = _.get("dataset")
    label = _.get("label")
    for key in dataset:
        plt.close('all')
        # medie - sd
        freq = {}
        mean = dataset[key].mean()
        sd = dataset[key].std()
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_title('Outliere %s; Set %s'%(key, label), fontsize=20)
        for instance in dataset[key]:
            # Cu verde cele care nu sunt outliere
            # cu magenta cele care sunt la 1 sd
            # cu rosu cele care sunt la 2 sd
            # cu negru cele care sunt la 3
            min_val = mean - 3 * sd
            max_val = mean + 3 * sd
            c = 'g'
            if   (mean - sd) <= instance <= (mean + sd):
                c = 'g'
            elif (mean - 2*sd) <= instance <= (mean + 2*sd):
                c = 'm'
            elif (mean - 3*sd) <= instance <= (mean + 3*sd):
                c = 'r'
            else:
                c = 'black'
            # colors = ['r','m','g']
            # #if not (min_val <= instance <= max_val):
            # for k in range(3):
            #     if not (mean - (k+1)*sd <= instance <= mean + (k+1)*sd): 
            #         c = colors[k]
            ax.scatter(instance, freq.get(instance, 0), c=c, s=50)
            freq[instance] = freq.get(instance, 0) + 0.1
        plt.savefig('./plots/outliers_mean_sd_%s_%s.jpg'%(key, label))

######## Outliere bivaraiate ########
# Folosind distanta Mahalanobis, doua cate doua, pentru cele 4 seturi de date (all data, doar supravietuitori, doar decedati, si datasetul extra)
from mahalanobis_tresholds import tresholds
train_truncated = train[['Survived', 'Age', 'Fare', 'Parch', 'Sex','SibSp']]
thyroid_truncated = thyroid[['A','B','C','D','E','F']]
DATASETS =  [
    {"label" : "all_data", "dataset": train_truncated}, 
    {"label" : "only_survived", "dataset" : train_truncated.loc[train['Survived'] == 1]}, 
    {"label" : "only_deceased", "dataset" : train_truncated.loc[train['Survived'] == 0]},
    {"label" : "thyroid", "dataset": thyroid_truncated}
]
for _ in DATASETS:
    dataset = _.get("dataset")
    label = _.get("label")
    # keys = train_truncated.columns
    keys = dataset
    for key_idx_i in range(len(keys) - 1):
        for key_idx_j in range(key_idx_i + 1, len(keys)):
            try:
                plt.close('all')
                keyi = keys[key_idx_i]
                keyj = keys[key_idx_j]
                tmp_dataset = dataset[[keyi, keyj]].values
                Sx = np.cov(tmp_dataset.T) #tmp_dataset.cov().values
                Sx = scipy.linalg.pinv(Sx) # Folosim inversa MoorePenrose, pentru ca cateodata avem matrici singulare. Daca matricea nu e singulara, A+ = A^-1, deci nu incurca.


                plot_name = 'mahalanobis_%s_%s_%s.jpg'%(keyi, keyj, label)
                # treshold = tresholds[keyi + '_' + keyj + '_' + label]
                treshold = 3 # consideram outliere cele mai n distantate.
                # calculam punctul mediu, pe aceste doua atribute
                mean_point = [dataset[keyi].mean(), dataset[keyj].mean()]
                plt.scatter(mean_point[0], mean_point[1], c='r', marker='P')
                
                max_distance = 0
                min_distance = np.Infinity

                distances = []
                for a, b in zip(dataset[keyi], dataset[keyj]):
                    mini_instance = np.array([a,b])
                    
                    d = scipy.spatial.distance.mahalanobis(mini_instance, mean_point, Sx) ** 2
                    distances.append(d)
                plt.scatter(dataset[keyi], dataset[keyj], c=distances, s=20, cmap='spring')
                plt.savefig('./plots/' + plot_name)
            except:
                continue

    # si acum scoatem outlierele, fara plot, luand toate cheile.
for _ in DATASETS:
    try:
        mean_point = dataset.mean().values
        Sx = np.cov(dataset.values.T)
        Sx = scipy.linalg.pinv(Sx)
        distances = []
        max_distance = 0
        min_distance = np.Infinity
        for row in dataset.iterrows():
            index, data = row
            instance = data.tolist()

            d = scipy.spatial.distance.mahalanobis(instance, mean_point, Sx) ** 2
            distances.append(d)
            if d > max_distance:
                max_distance = d
            if d < min_distance:
                min_distance = d
        treshold = max_distance * 0.85
        print("Outliere pentru label %s; %f - %f"%(label, min_distance, max_distance))
        # print(distances)
        for idx, d in enumerate(distances):
            if d > treshold:
                print(dataset.iloc[[idx]], d)

        #https://stackoverflow.com/questions/29817090/is-there-a-python-equivalent-to-the-mahalanobis-function-in-r-if-not-how-can
    except:
        continue


# Local Outlier Factor

model = LocalOutlierFactor(n_neighbors=20)
x = train[['Age', 'Fare', 'Parch', 'Sex','SibSp', 'Survived']]
predictions = model.fit_predict(x)
# Vom afisa outlierele in urmatoarele grafice 2d, pentru a fi mai usor de observat si probabil mai interesante (patram doar cele continue, nu categoriale):
# (Cu cercuri verzi vom prezenta supravietuitorii, rosii cei decedati, iar in loc de cercuri folosim stelute pentru outliere)
# x = Age, y = Fare
plt.close('all')
COLORS = ['r', 'g']
for idx, prediction in enumerate(predictions):
    full_data = x.iloc[[idx - 1]]
    xc = full_data['Age'].values[0]
    yc = full_data['Fare'].values[0]
    # print(x, y)
    color = COLORS[int(full_data['Survived'].values[0])]
    marker = '.'
    if prediction == -1:
        marker = '*'
    plt.scatter(xc, yc, c=color, marker=marker)
plt.savefig('./plots/LOF_Age_Fare.pdf')

# x = Fare, y = Parch + SibSp
plt.close('all')
for idx, prediction in enumerate(predictions):
    full_data = x.iloc[[idx]]
    xc = full_data['Fare'].values[0]
    yc = full_data['Parch'].values[0] + full_data['SibSp'].values[0]
    color = COLORS[int(full_data['Survived'].values[0])]
    marker = '.'
    if prediction == -1:
        marker = '*'
    plt.scatter(xc, yc, c=color, marker=marker)
plt.savefig('./plots/LOF_Fare_FamilySize.pdf')

# Aici, nu facem grafic pentru ca datele nu au label-uri si nu stim ce inseamna. Pur si simplu vedem cate din cele gasite de noi ca fiind outliere chiar sunt.

model = LocalOutlierFactor(n_neighbors=40)
x = thyroid[['A','B','C','D','E','F']]
predictions = model.fit_predict(x)
plt.close('all')
COLORS = ['r', 'g']
print("LOF vs actual pentru Thyroid dataset")
for idx, prediction in enumerate(predictions):
    actual = int(thyroid.iloc[[idx]]['Y'].values[0])
    if prediction == 1 and actual == 1:
        # Nu e outlier pentru LOF, dar e de fapt
        print("Cel de pe pozitia %d e outlier doar in date, nu si pentru LOF"%(idx))
    if prediction == -1 and actual == 0:
        # LOF crede ca e outlier, dar nu e.
        print("Cel de pe pozitia %d e outlier doar pentru LOF, nu si in date"%(idx))



# # Pentru Thyroid vom proceda la fel pentru Titanic. Vom afisa in functie de toate perechile de axe.
# # Vom repreeznta outlierele cu rosu, celelalte cu verde.
# model = LocalOutlierFactor(n_neighbors=40)
# x = thyroid[['A','B','C','D','E','F']]
# predictions = model.fit_predict(x)
# keys = ['A','B','C','D','E','F']
# idx_plot = 1
# for key_idx_1 in range(len(keys) - 1):
#     for key_idx_2 in range(key_idx_1, len(keys)):
#         key1 = keys[key_idx_1]
#         key2 = keys[key_idx_2]
#         plt.close('all')
#         plt.scatter(x[key1], x[key2], c=predictions)
#         plt.title('LOF Thyroid %d'%(idx_plot))
#         plt.xlabel(key1)
#         plt.ylabel(key2)
#         idx_plot += 1
#         plt.show()