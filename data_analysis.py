# Lab1

import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

train = pd.read_csv('train.csv')

#Facem cateva modificari la date, pentru a fi numerice, sau cele null le inlocuim.

train['Embarked'] = train['Embarked'].fillna('S')
train['Embarked'] = train['Embarked'].map({'S' : 0, 'C' : 1, 'Q' : 2}).astype(int)
train['Fare'] = train['Fare'].fillna(train['Fare'].median())
train['Sex'] = train['Sex'].map( {'female': 0, 'male': 1} ).astype(int)
train = train.fillna(0)
#Eliminam unele inutile
drop_elements = ['PassengerId', 'Name', 'Ticket', 'Cabin']
train = train.drop(drop_elements, axis = 1)

train.describe()


print("######### Analiza univariata #########")
print("1.1. Tendinte centrale - Media")
print(train.mean())
print("1.2. Tendinte centrale - Mediana")
print(train.median())

print("2. Quartilele")
for key in ['Age', 'SibSp', 'Parch']:
	try:
		print(train[key].to_frame().quantile([0.25, 0.5, 0.75]))
	except:
		continue

print("3. Varianta")
print(train.var())

print("4. Kurtosis")
print(train.kurtosis())

print("5. Skewness")
print(train.skew())



plt.close('all')
train.hist()
plt.savefig('./plots/attribute_histograms.png')
plt.close('all')
for key in train.columns:
	plt.close('all')
	train[key].to_frame().boxplot()
	plt.savefig('./plots/boxplot_%s'%(key))

for key in train.columns:
	try:
		plt.close('all')
		train[key].to_frame().plot.density()
		plt.savefig('./plots/density_%s'%(key))
	except:
		continue

# Analiza bivariata
# Corelarea pearson a atributelor
colormap = plt.cm.RdBu
plt.figure(figsize=(14,12))
plt.title('Corelarea Pearson', y=1.05, size=15)
sns.heatmap(train.astype(float).corr(), linewidths=0.1, vmax=1.0, square=True, cmap=colormap, linecolor='white', annot=True)
plt.savefig('./plots/pearson_correlation.png')
plt.close('all')

sns.pairplot(train)
plt.savefig('./plots/pairplot.png')


# Incercam sa facem PCA.
features = ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']
x = train.loc[:, features].values
y = train.loc[:, ['Survived']].values
x = StandardScaler().fit_transform(x)

pca = PCA(n_components=2)
principal_components = pca.fit_transform(x)
principal_dataframe = pd.DataFrame(data = principal_components, columns=['Componenta principala 1','Componenta principala 2'])
final_df = pd.concat([principal_dataframe, train[['Survived']]], axis=1)

plt.close('all')
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Componenta principala 1', fontsize=15)
ax.set_ylabel('Componenta principala 2', fontsize=15)
ax.set_title('PCA cu 2 componente', fontsize=20)

targets = [0, 1]
colors = ['r','g']
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'Componenta principala 1'], final_df.loc[indices_to_keep, 'Componenta principala 2'], c=color, s=50)
ax.legend(targets)
ax.grid()
plt.savefig('./plots/2PCA.jpg')


# PCA cu 3 componente.
pca = PCA(n_components=3)
principal_components = pca.fit_transform(x)
principal_dataframe = pd.DataFrame(data = principal_components, columns=['PC1','PC2', 'PC3'])
final_df = pd.concat([principal_dataframe, train[['Survived']]], axis=1)

plt.close('all')
fig = plt.figure()
targets = [0,1]
colors = ['r','g']

ax = fig.add_subplot(111, projection='3d')
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'PC1'], final_df.loc[indices_to_keep, 'PC2'], final_df.loc[indices_to_keep, 'PC3'], c=color, cmap='Set2_r', s=50)
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
ax.set_zlabel('PC3')
ax.set_title('PCA cu 3 componente')
# plt.show(block=True)
plt.savefig('./plots/3PCA.jpg')


# mai trebuie mapari, projection, boxploturi conditionate

# Mapare TSNE cu 2 elemente
plt.close('all')
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_title('TSNE cu 2 componente', fontsize=20)
features = ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']
x = train.loc[:, features].values
train_embedded = TSNE(n_components=2).fit_transform(x)
train_embedded_df = pd.DataFrame(data = train_embedded, columns=['x','y'])
final_df = pd.concat([train_embedded_df, train[['Survived']]], axis=1)
targets = [0, 1]
colors = ['r','g']
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'x'], final_df.loc[indices_to_keep, 'y'], c=color, s=50)
ax.legend(targets)
ax.grid()
plt.savefig('./plots/TSNE_2.jpg')

# TSNE cu 3 elemente.
plt.close('all')
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_zlabel('z', fontsize=15)
ax.set_title('TSNE cu 3 componente', fontsize=20)
features = ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']
x = train.loc[:, features].values
train_embedded = TSNE(n_components=3).fit_transform(x)
train_embedded_df = pd.DataFrame(data = train_embedded, columns=['x','y', 'z'])
final_df = pd.concat([train_embedded_df, train[['Survived']]], axis=1)
targets = [0, 1]
colors = ['r','g']
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'x'], final_df.loc[indices_to_keep, 'y'], final_df.loc[indices_to_keep, 'z'], c=color, s=50)
ax.legend(targets)
ax.grid()
plt.savefig('./plots/TSNE_3.jpg')
#plt.show(block=True)

# Projection Pursuit - 2 elements
from skpp import ProjectionPursuitRegressor
estimator = ProjectionPursuitRegressor(r=2)
features = ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']
x = train.loc[:, features].values
y = train.loc[:,['Survived']].values
plt.close('all')
estimator.fit_transform(x, y)
trained_df = pd.DataFrame(data = estimator.fit_transform(x, y), columns=['x','y'])
final_df = pd.concat([trained_df, train[['Survived']]], axis=1)
targets = [0, 1]
colors = ['r','g']
plt.close('all')
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(111)
ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_title('Projection Pursuit, 2 elemente', fontsize=20)
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'x'], final_df.loc[indices_to_keep, 'y'], c=color, s=50)
ax.legend(targets)
ax.grid()
plt.savefig('./plots/PP2.jpg')

plt.close('all')

# Projection Pursuit - 3 elements
estimator = ProjectionPursuitRegressor(r=3)
features = ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']
plt.close('all')
estimator.fit_transform(x, y)
trained_df = pd.DataFrame(data = estimator.fit_transform(x, y), columns=['x','y', 'z'])
final_df = pd.concat([trained_df, train[['Survived']]], axis=1)

fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('x', fontsize=15)
ax.set_ylabel('y', fontsize=15)
ax.set_zlabel('z', fontsize=15)
ax.set_title('Projection Pursuit, 3 elemente', fontsize=20)
for target, color in zip(targets, colors):
	indices_to_keep = final_df['Survived'] == target
	ax.scatter(final_df.loc[indices_to_keep, 'x'], final_df.loc[indices_to_keep, 'y'], final_df.loc[indices_to_keep, 'z'], c=color, s=50)
ax.legend(targets)
ax.grid()
plt.savefig('./plots/PP3.jpg')

plt.close('all')

# Boxplot-uri conditionale
for key in train.columns:
	plt.close('all')
	train.boxplot(column=key, by='Survived')
	plt.savefig('./plots/boxplot_conditional_%s.jpg'%(key))